# Makefile for PyMake, by Geoff Samuel

# Module Name		:	rigControlMaker
# Description		:	A module for creating helper shapes as rig controllers
# Author			:	Geoff Samuel
# Support			:	Support@GeoffSamuel.com
# Website			:	www.GeoffSamuel.com

version=2.0.1

target=PYTHON,OTHER

target=PYTHON,OTHER
stringSubstitute=!VERSION!=$CALL(version)
centralDeploy=C:/pythonTools/deployed/$CALL(toolname)/versions/$CALL(versionToPy($CALL(version)))/
deployment=$CALL(fromEnv(PYMAKE_INSTALL))

dependencies=pyBase

#############
# PYTHON
#############
convertPyQtUI=True
pythonSrc=python/

#############
# OTHER (XML)	
#############
otherSrc=python/
otherTypes=xml,txt