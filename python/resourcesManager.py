""" Resource handling class
"""

import inspect
import os


class ResourcesManager(object):
	""" Resource Manager, a class to handle additional files in a given files.
		This logic is pretty generic so it may move to PyBase at some point from this project
	"""
	def __init__(self, resoucesFolder="resources", fileTypeFilters=None, autoLoad=True):
		""" Constructor

			Args:
				resourcesFolder (string)	: name of the folder, relative to the module, where the
											  resources files should be sourced from
				fileTypeFilers (list string): list of stings of the deseried files types wanted.
				autoLoad (bool)				: automaiclly search the resources folder after 
											  construsted
		"""
		super(ResourcesManager, self).__init__()
		self.currentLoc = os.path.dirname(
										os.path.abspath(
													inspect.getfile(
																inspect.currentframe()
																)
													)
										)
		self.typeFilters = fileTypeFilters
		self.__resoucesFolder = resoucesFolder

		self.resouces = {}

		if autoLoad:
			self.loadResources()

	def getResourcesRootPath(self):
		""" returns the current path to the resouces folder
		"""
		return os.path.join(self.currentLoc, self.__resoucesFolder)

	def loadResources(self):
		""" Check the resources folder for all matching files. Use getResources() to get all the
			matching files
		"""
		tempResouces = {}
		for root, _, files in os.walk(os.path.join(self.currentLoc, self.__resoucesFolder)):
			for file_ in files:
				if self.typeFilters is None:
					name = os.path.basename(file_)
					tempResouces[name] = file_
				else:
					for fileType in self.typeFilters:
						if file_.endswith(fileType):
							name = os.path.basename(file_)
							tempResouces[name] = os.path.join(root, file_)
		self.resouces = tempResouces

	def getResources(self):
		""" Return a list of strings of all the found files, and their full paths
		"""
		return self.resouces
