""" Objects to create a custom shapes for use with rigging
"""
import maya.cmds as cmds
import maya.OpenMayaUI as mui
from PyQt4 import QtGui, QtCore
import sip

from rigControlMaker import rigResources
from rigControlMaker.ui import ui_rigControlMaker 


def _getMayaMainWindow():
	""" Maya method to get a pointer handler 
	""" 
	ptr = mui.MQtUtil.mainWindow()
	return sip.wrapinstance(long(ptr), QtCore.QObject) 


class ShapeHandler(object):
	""" Class to get the shapes from the data store, and to construct them
	"""
	def __init__(self):
		super(ShapeHandler, self).__init__()

	def constructShape(self, shapeClass):
		""" Construct the shape from the given shape name.
			Will return a maya node if successful
		"""
		mayaShape = cmds.curve(d=shapeClass.degree, p=shapeClass.knots)
		if shapeClass.closedShape:
			cmds.closeCurve(mayaShape, rpo=True, ps=True)

		mayaShape = cmds.rename(mayaShape, shapeClass.name)

		return mayaShape

	def saveShapeFromScene(self, shapeObjectName, shapeName):
		""" Convert the requested shape into a dict that can be used to save out the shape
		"""
		shapeDict = {}
		shapeDict["name"] = shapeName

		# Closed Shape
		isClosed = "True"
		if cmds.getAttr("%s.form" % shapeObjectName) == 0:
			isClosed = "False"
		shapeDict["closedShape"] = isClosed

		# Scale
		degree = cmds.getAttr("%s.d" % shapeObjectName)
		shapeDict["degree"] = "%s" % degree

		# Do some Checks here
		allPoints = []
		points = cmds.getAttr("%s.cv[:]" % shapeObjectName)
		for point in points:
			for coord in point:
				allPoints.append("%s" % coord)
		convertedPoints = ",".join(allPoints)

		shapeDict["knots"] = convertedPoints
		return shapeDict


class QStringValidator(QtGui.QValidator):
	""" Wrapper for a Validator Function to be a QValidator
		Can see this being moved into PyBase in the near future
	"""
	def __init__(self, inputCallable, parent=None):
		""" Constructor,
			inputCallable is the function to call to validate the string
		"""
		super(QStringValidator, self).__init__(parent=parent)
		self.__validateMethod = inputCallable
	
	def validate(self, input, pos):
		""" Validate the input string
		"""
		if self.__validateMethod(input):
			return (QtGui.QValidator.Acceptable, pos)
		else:
			return (QtGui.QValidator.Invalid, pos)


class RigControlMaker(QtGui.QDialog):
	""" main UI Class
	"""
	def __init__(self, parent=None):
		parent = parent or _getMayaMainWindow()
		super(RigControlMaker, self).__init__(parent=parent)
		self.ui = ui_rigControlMaker.Ui_Dialog()
		self.ui.setupUi(self)
		self.setWindowTitle("Rig Helper Menu")
		self.setObjectName('Rig Helper')
		self.stringValidator = None
		self.rigLib = rigResources.RigLibrary()
		self.shapeFactory = ShapeHandler()
		self.__establishConnections()

	def __populateLibraryList(self):
		""" Get the libraries from the rigSources and populate the dropdown box
		"""
		mtModel = QtGui.QStandardItemModel(1, 1)
		self.ui.CurrentShapeSet_combo.setModel(mtModel)
		self.ui.deleteShapeSet_ComboBox.setModel(mtModel)
		libs = self.rigLib.getRigLibraries()
		for idx in range(len(libs)):
			mtModel.setItem(idx, 0, QtGui.QStandardItem(libs[idx]))

	def __populateShapeList(self):
		""" Method to fill the shape name drop down and main list box widgets
		"""
		#build all the items for the list box based off the list at the top
		mtModel = QtGui.QStandardItemModel(1, 1)
		self.ui.RigHelper_ListView.setModel(mtModel)
		self.ui.deleteShape_ComboBox.setModel(mtModel)
		names = self.rigLib.getShapeNames()
		for idx in range(len(names)):
			mtModel.setItem(idx, 0, QtGui.QStandardItem(names[idx]))
		self.__libChange(0, repopulateShapeList=False)

	def __libChange(self, index, repopulateShapeList=True):
		""" Method to update the UI drop down with all the shape libaries
		"""
		self.rigLib.setRigLibrary(str(self.ui.CurrentShapeSet_combo.currentText()))
		if repopulateShapeList:
			self.__populateShapeList()

	def saveSelectedShape(self, shapeName=None):
		""" Save the currently selected shape to the currently loaded shape libary

			Args:
				shapeName (string)	: the name to save the selected shape as
		"""
		#get selected
		sel = cmds.ls( selection=True )
		#make sure something is selected
		if(len(sel) == 0):
			print "No Object Selected"
			return
		newShapeName = shapeName or str(self.ui.SaveSelectedShape_lineEdit.text())
		newShapeDict = self.shapeFactory.saveShapeFromScene(sel[0], newShapeName)
		self.rigLib.storeNewShape(newShapeDict)
		self.__libChange(0)

	def __establishConnections(self):
		""" Internal function to link up the UI with the functions within the class
		"""
		self.ui.versionTextBox.setText("!VERSION!")
		self.ui.Create_Origin_Button.clicked.connect(self.createAtOrigin)
		self.ui.Create_Selected_Button.clicked.connect(self.createAtSelected)
		self.ui.Create_Manual_Button.clicked.connect(self.createAtManual)

		self.ui.SaveSelectedShape_Button.clicked.connect(self.saveSelectedShape)

		self.ui.NewSet_Button.clicked.connect(self.createNewSet)
		self.ui.deleteShapeSet_Button.clicked.connect(self.deleteSet)
		self.ui.deleteShape_Button.clicked.connect(self.deleteShape)

		self.ui.CurrentShapeSet_combo.currentIndexChanged.connect(self.__libChange)

		self.__populateLibraryList()

		self.stringValidator = QStringValidator(self.rigLib.validateName)
		self.ui.SaveSelectedShape_lineEdit.setValidator(self.stringValidator)

	def createNewSet(self):
		""" Create a new shape libaray with the name currently in the new set name text box
		"""
		self.rigLib.newRigLibrary(str(self.ui.NewSetName_lineEdit.text()))
		self.__populateLibraryList()

	def deleteSet(self):
		""" Delete the currently selected shape libary
		"""
		self.rigLib.deleteRigLibrary(str(self.ui.deleteShapeSet_ComboBox.currentText()))
		self.__populateLibraryList()
	
	def deleteShape(self):
		""" Delete the currently selected shape from the shape libary
		"""
		self.rigLib.removeShapeFromLibrary(str(self.ui.deleteShape_ComboBox.currentText()))
		self.__libChange(0)

	def __getSelectedObject(self):
		""" Internal function to return the selected item as a string
		"""
		return self.ui.RigHelper_ListView.selectedIndexes()[0].data().toString()

	def createObject(self, name):
		""" Creates the shape from the shapeFactory based off the supplied name
		"""
		shapeObj = self.rigLib.getShape(str(name))
		newShape = self.shapeFactory.constructShape(shapeObj)
		return newShape

	def createAtOrigin(self):
		""" Creates the selected shape at Origin (0, 0, 0)
		"""
		self.createObject(self.__getSelectedObject())

	def createAtSelected(self):
		""" Creates the selected shape at the location of the seleted object
		"""
		#get selected
		sel = cmds.ls( selection=True )
		#make sure something is selected
		if(len(sel) == 0):
			print "No Object Selected"
			return
		#get the location of the first object
		newLoc = cmds.xform(sel[0], q=1, ws=1, rp=1)
		newObj = self.createObject(self.__getSelectedObject())
		cmds.xform(newObj, ws=1, t=newLoc)

	def createAtManual(self):
		""" Creates the selected shape in the given coordinates
		"""
		newObj = self.createObject(self.__getSelectedObject())
		#Get the X Y Z values from Qt and use them to set the location of the new object
		xVal = self.ui.X_Spiner.value()
		yVal = self.ui.Y_Spiner.value()
		zVal = self.ui.Z_Spiner.value()
		cmds.xform(newObj, ws=1, t=(xVal, yVal, zVal))


if __name__ == "__main__":
	rigControl = RigControlMaker()
	rigControl.show()