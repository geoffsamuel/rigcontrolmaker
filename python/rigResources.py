import os
import re
from xml.etree import ElementTree as et

from rigControlMaker import resourcesManager


class _ShapeInfo(object):
	""" Class to handle the conversion from a xml object to a python object we can use in Maya
	"""
	def __init__(self):
		""" Constructor
		"""
		self.name = None
		self.closedShape = None
		self.degree = None
		self.knots = None

	def __convertDegree(self, inputText):
		""" Internal Method 
			Convert the text to an Int
		"""
		try:
			return int(inputText)
		except ValueError:
			raise ValueError("Unable to convert value to Int")
	
	def __convertClosedShaped(self, inputText):
		""" Internal Method
			Converts the intput bool value to a python bool value
		"""
		if inputText.lower() == "true":
			return True
		else:
			return False
	
	def __convertKnots(self, inputText):
		""" Internal Method
			convert a string of commer seprated floats to a list of float values
		"""
		splitText = inputText.split(",")
		points = []

		for idx in xrange(0, len(splitText), 3):
			if idx + 3 > len(splitText):
				raise ValueError("Incorrect number of points found")
			try:
				points.append(
								(
								float(splitText[idx]),
								float(splitText[idx + 1]),
								float(splitText[idx + 2]),
								),
							)
			except ValueError:
				raise ValueError("Unable to convert value to float")
		return points

	def getFromXML(self, xmlElement):
		""" Build the shape info from a row in an xml file
			
			Args:
				xmlElement (xmlRowElement)	: The xml row to 
		"""
		self.name = xmlElement.findtext("name")
		self.closedShape = self.__convertClosedShaped(xmlElement.findtext("closedShape"))
		self.degree = self.__convertDegree(xmlElement.findtext("degree"))
		self.knots = self.__convertKnots(xmlElement.findtext("knots"))


class RigLibrary(object):
	""" Main rigging libaray.
		This class will hand the resources and translation between the XML and python objects, 
		as well as manage the shapes (adding/ removing) and the libaries (adding/ removing)
	"""
	def __init__(self):
		super(RigLibrary, self).__init__()
		self.__resMan = None
		self.refreshLibrary()
		self.__shapes = {}
		self.__currentRigLib = None

	def refreshLibrary(self):
		""" Refresh the libray resources cache
		"""
		self.__resMan = resourcesManager.ResourcesManager(
														resoucesFolder="resources", 
														fileTypeFilters=(".xml",)
														)

	def newRigLibrary(self, newName):
		""" Create a new library uing the given name
		
			Args:
				newName (str)	: the name of the new library
		"""
		newPathName = os.path.join(self.__resMan.getResourcesRootPath(), newName)
		openFile = open("%s.xml" % newPathName,'w')
		openFile.writelines(["<?xml version=\"1.0\"?>", "<shapes>", "</shapes>"])
		openFile.close()
		self.refreshLibrary()

	def deleteRigLibrary(self, libraryToRemove):
		""" Remove the requested library

			Args:
				libraryToRemove (str)	: the name of the libray to remove
		"""
		filePath = os.path.join(self.__resMan.getResourcesRootPath(), libraryToRemove)
		if os.path.exists(filePath):
			os.remove("%s.xml" % filePath)
			self.refreshLibrary()

	def removeShapeFromLibrary(self, shapeName):
		""" Remove the requested shape from the currently active library

			Args:
				shapeName (string)	: the name of the shape to remove
		"""
		shapeFile = et.parse(self.__currentRigLib)

		root = shapeFile.getroot()
		shapes = root.findall("shape")
		
		for shape in shapes:
			name = shape.findtext("name")
			if name == shapeName:
				root.remove(shape)
		shapeFile.write(self.__currentRigLib)

	def getRigLibraries(self):
		""" Return a list of all the shape library that the resources manager is awear of
		"""
		return [shapeLib.replace(".xml", "", 1) for shapeLib in self.__resMan.getResources()]

	def validateName(self, stringToValidate):
		""" Validate a shape name, as its used in maya, we want to ensure the name stored is valid

			Args:
				stringToValidate (string)	: the string to validate

			Returns:
				True if the string is valid, False if the string is not Valid
		"""
		pattern = r'^[a-zA-Z0-9_]*$'
		validator = re.compile(pattern)
		if validator.match(stringToValidate) is None:
			return False
		else:
			return True

	def setRigLibrary(self, libraryName):
		""" Method to set the current active library
		
			Args:
				libraryName (string)	: string name to set the active library
		"""
		self.__shapes = {}
		libResources = self.__resMan.getResources()
		libraryName = "%s.xml" % libraryName
		if libraryName not in libResources.keys():
			return None

		self.__currentRigLib = libResources[libraryName]

		shapeFile = et.parse(self.__currentRigLib)

		root = shapeFile.getroot()
		shapes = root.findall("shape")

		for shape in shapes:
			newShape = _ShapeInfo()
			newShape.getFromXML(shape)
			self.__shapes[newShape.name] = newShape

	def getShape(self, shapeName):
		""" Get the requested shape as a shape object

			Args:
				shapeName (string)	: name of the shape
		
			Returns:
				A shape object created from the shape in the active library
		"""
		return self.__shapes[shapeName]

	def getShapeNames(self):
		""" Get the shape names in the current active library

			Returns:
				A list of strings, which are the names of the shapes in the active library
		"""
		return [shape for shape in self.__shapes.keys()]

	def storeNewShape(self, newShapeDict):
		""" Store a new shape in the currently active Library

			Args:
				newShapeDict (Dict)		: A dictonary discribing the shape. Required properites are:
											name		: String name of the shape
											closedShape	: string bool value if the shape is closed
											degree		: a sting int of the degree of the shape
											knots		: a string of commer joined floats that
														  make up the XYZ location of each point
		"""
		if not self.validateName(newShapeDict['name']):
			raise ValueError("Invalid Shape Name")

		shapeFile = et.parse(self.__currentRigLib)

		root = shapeFile.getroot()

		tempBase = et.Element("shape")

		tempName = et.Element("name")
		tempName.text = newShapeDict["name"]
		
		tempClosedShape = et.Element("closedShape")
		tempClosedShape.text = newShapeDict["closedShape"]
		
		tempDegree = et.Element("degree")
		tempDegree.text = newShapeDict["degree"]
		
		tempKnots = et.Element("knots")
		tempKnots.text = newShapeDict["knots"]
		
		tempBase.append(tempName)
		tempBase.append(tempClosedShape)
		tempBase.append(tempDegree)
		tempBase.append(tempKnots)

		root.append(tempBase)

		shapeFile.write(self.__currentRigLib)
