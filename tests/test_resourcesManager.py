import os
from xml.etree import ElementTree as et

from pyBase.testing import testing 
from rigControlMaker import rigResources

class FakeResourceManager(object):
	def __init__(self, resourcesFolder=None, resources=None):
		self.resourcesFolder = resourcesFolder
		self.resources = resources

	def getResourcesRootPath(self):
		return self.resourcesFolder

	def getResources(self):
		return self.resources

def createWholeXMLLibrary():
	pass


def createXMLShape(name=None, closed=None, degree=None, knots=None):
	name = name or "DummyShape"
	closed = closed or "True"
	degree = degree or "5"
	knots = knots or "1.0,1.0,1.0,2.0,2.0,2.0,3.0,3.0,3.0"

	tempBase = et.Element("shape")

	tempName = et.Element("name")
	tempName.text = name

	tempClosedShape = et.Element("closedShape")
	tempClosedShape.text = closed

	tempDegree = et.Element("degree")
	tempDegree.text = degree

	tempKnots = et.Element("knots")
	tempKnots.text = knots

	tempBase.append(tempName)
	tempBase.append(tempClosedShape)
	tempBase.append(tempDegree)
	tempBase.append(tempKnots)

	return tempBase

class Test_ShapeInfo(testing.PyBaseTestingClass):	
	def setUp(self):
		super(Test_ShapeInfo, self).setUp()
		self.shape = rigResources._ShapeInfo()
	
	def tearDown(self):
		super(Test_ShapeInfo, self).tearDown()
		self.shape = None
	
	def test_constructor(self):
		newItem = rigResources._ShapeInfo()
		self.assertEquals(newItem.name, None)
		self.assertEquals(newItem.closedShape, None)
		self.assertEquals(newItem.degree, None)
		self.assertEquals(newItem.knots, None)

	def test_convertDegreeInt(self):
		expectedResult = 123
		input = "123"
		result = self.shape._ShapeInfo__convertDegree(input)
		self.assertEquals(result, expectedResult)

	def test_convertDegreeInvalidFloat(self):
		input = "123.456"
		self.assertRaises(
			ValueError, 
			self.shape._ShapeInfo__convertDegree, 
			input
		)

	def test_convertDegreeInvalidString(self):
		input = "AbCdEfG"
		self.assertRaises(
			ValueError, 
			self.shape._ShapeInfo__convertDegree, 
			input
		)

	def test_convertClosedShapeBool(self):
		expectedResult = True
		input = "True"
		result = self.shape._ShapeInfo__convertClosedShaped(input)
		self.assertEquals(result, expectedResult)

	def test_convertClosedShapeBoolAllCaps(self):
		expectedResult = True
		input = "TRUE"
		result = self.shape._ShapeInfo__convertClosedShaped(input)
		self.assertEquals(result, expectedResult)

	def test_convertClosedShapeBoolAllLower(self):
		expectedResult = True
		input = "true"
		result = self.shape._ShapeInfo__convertClosedShaped(input)
		self.assertEquals(result, expectedResult)

	def test_convertClosedShapeBoolFalse(self):
		expectedResult = False
		input = "false"
		result = self.shape._ShapeInfo__convertClosedShaped(input)
		self.assertEquals(result, expectedResult)

	def test_convertKnotsAsExpected(self):
		expectedResult = [(1.0, 2.0, 3.0),]
		input = "1.0,2.0,3.0"
		result = self.shape._ShapeInfo__convertKnots(input)
		self.assertEquals(result, expectedResult)

	def test_convertKnotsAsExpectedLonger(self):
		expectedResult = [
						(1.0, 2.0, 3.0),
						(1.0, 2.0, 3.0),
						(1.0, 2.0, 3.0),
						]
		input = "1.0,2.0,3.0,1.0,2.0,3.0,1.0,2.0,3.0"
		result = self.shape._ShapeInfo__convertKnots(input)
		self.assertEquals(result, expectedResult)

	def test_convertKnotsBadNumberOfPoints(self):
		input = "1.0"
		self.assertRaises(
			ValueError, 
			self.shape._ShapeInfo__convertKnots, 
			input
		)

	def test_convertKnotsWithSpaces(self):
		expectedResult = [(1.0, 2.0, 3.0),]
		input = "1.0, 2.0, 3.0"
		result = self.shape._ShapeInfo__convertKnots(input)
		self.assertEquals(result, expectedResult)

	def test_convertKnotsBadString(self):
		input = "ABC"
		self.assertRaises(
			ValueError, 
			self.shape._ShapeInfo__convertKnots, 
			input
		)

	def test_convertKnotsBadMixedString(self):
		input = "1.0,2.0,3.0,4.0,ABC,5.0"
		self.assertRaises(
			ValueError, 
			self.shape._ShapeInfo__convertKnots, 
			input
		)

	def test_convertKnotsWithInts(self):
		expectedResult = [(1.0, 2.0, 3.0),(4.0, 5.0, 6.0),]
		input = "1,2,3,4,5,6"
		result = self.shape._ShapeInfo__convertKnots(input)
		self.assertEquals(result, expectedResult)

	def test_readFromXML(self):
		inName = "ThisIsADummyShape"
		inClosed = "False"
		inDegree = "2"
		inKnots = "1.0,2.0,3.0,4.0,5.0,6.0"

		expectedClosed = False
		expectedDegree = 2
		expectedKnots = [(1.0, 2.0, 3.0,),(4.0, 5.0, 6.0)]

		input = createXMLShape(
							name=inName,
							closed=inClosed,
							degree=inDegree,
							knots=inKnots
							)
		self.shape.getFromXML(input)
		
		self.assertEquals(self.shape.name, inName)
		self.assertEquals(self.shape.closedShape, expectedClosed)
		self.assertEquals(self.shape.degree, expectedDegree)
		self.assertEquals(self.shape.knots, expectedKnots)
